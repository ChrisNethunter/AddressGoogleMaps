package com.example.root.googlemaps.Modules;

import java.util.List;

/**
 * Created by root on 30/11/17.
 */

public interface DirectionFinderListener {
    void onDirectionFinderStart();
    void onDirectionFinderSuccess(List<Route> route);
}
