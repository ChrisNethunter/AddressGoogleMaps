package com.example.root.googlemaps.Modules;

/**
 * Created by root on 30/11/17.
 */

public class Distance {

    public String text;
    public int value;

    public Distance(String text, int value) {
        this.text = text;
        this.value = value;
    }
}
