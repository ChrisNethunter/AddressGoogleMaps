package com.example.root.googlemaps;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;


import com.example.root.googlemaps.Modules.DirectionFinder;
import com.example.root.googlemaps.Modules.DirectionFinderListener;
import com.example.root.googlemaps.Modules.Route;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener, DirectionFinderListener {

    private GoogleMap mMap;
    private Marker marcador;

    Geocoder geocoder;
    List<Address> addresses;

    double lat = 0.0;
    double lng = 0.0;

    double lat2 = 0.0;
    double lng2 = 0.0;


    double latStartCalc = 0.0;
    double lngStartCalc = 0.0;

    double latEndCalc = 0.0;
    double lngEndCalc = 0.0;

    int trigger = 0;


    private LocationManager locationManager;

    //Our buttons
    private FloatingActionButton buttonNewCalculate;
    private FloatingActionButton buttonCalcDistance;

    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();

    private ProgressDialog progressDialog;

    int MY_PERMISSION_LOCATION = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        fullScreen();

        final PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                //Log.i("DIRECCION", "Place: " + place.getName() +" "+ place.getLatLng());
                try {
                    setPointsMap(place.getLatLng());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                autocompleteFragment.setText("");
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("DIRECCION", "An error occurred: " + status);
            }
        });

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        geocoder = new Geocoder(this, Locale.getDefault());

        checkLocation();
        buttonNewCalculate = findViewById(R.id.buttonNewCalcDistance);
        buttonCalcDistance = findViewById(R.id.buttonCalcDistance);

        buttonNewCalculate.setOnClickListener(this);
        buttonCalcDistance.setOnClickListener(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {

            @Override
            public void onMapLongClick(LatLng position) {
                try {
                    //Log.d("clickmaps", "hereeeee " + position);
                    setPointsMap(position);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });


        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (Build.VERSION.SDK_INT >= 23) {
            marshmallowGPSPremissionCheck();

        } else {
            enableMyLocation();

        }

        miUbicacion();

    }

    private void marshmallowGPSPremissionCheck() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && checkSelfPermission(
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(
                    new String[]{
                            android.Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSION_LOCATION);
        } else {
            enableMyLocation();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSION_LOCATION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            enableMyLocation();
        }else {
            // permission denied, boo! Disable the
            // functionality that depends on this permission.
        }
    }

    private void enableMyLocation() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mMap.setMyLocationEnabled(true);
    }
    private void miUbicacion() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        Location location = locationManager.getLastKnownLocation(locationManager.GPS_PROVIDER);
        //agregarMarcadorFirts(location.getLatitude(), location.getLongitude());
        //actualizarUbicacion(location);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,15000,0,locListener);
    }

    private void setPointsMap(LatLng position) throws IOException {

        if (latStartCalc != 0.0){

            if (latEndCalc == 0.0){

                latEndCalc = position.latitude;
                lngEndCalc = position.longitude;

                addresses = geocoder.getFromLocation(latEndCalc, lngEndCalc, 1);
                String address = addresses.get(0).getAddressLine(0);


                addMarkerCalculate(latEndCalc, lngEndCalc, address);
            }
        }else{

            latStartCalc = position.latitude;
            lngStartCalc = position.longitude;

            addresses = geocoder.getFromLocation(latStartCalc, lngStartCalc, 1);
            String address = addresses.get(0).getAddressLine(0);

            addMarkerCalculate(latStartCalc, lngStartCalc, address);

        }
    }

    private void addMarkerCalculate(double latCalc , double lgnCalc , String message){

        LatLng position = new LatLng(latCalc, lgnCalc);

        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

        //Adding a new marker to the current pressed position
        mMap.addMarker(new MarkerOptions()
                .title(message)
                .position(position)
                .draggable(true));

    }

    private void calculateDistanceStartEnd(){

        if (latStartCalc == 0.0 && latEndCalc == 0.0){
            Toast toast = Toast.makeText(this, "Check Start and End Point ", Toast.LENGTH_LONG);
            toast.show();
        }else{
            Location loc1 = new Location("");
            loc1.setLatitude(latStartCalc);
            loc1.setLongitude(lngStartCalc);

            Location loc2 = new Location("");
            loc2.setLatitude(latEndCalc);
            loc2.setLongitude(lngEndCalc);

            float distanceInMeters = loc1.distanceTo(loc2);

            /*Polyline line = mMap.addPolyline(new PolylineOptions()
                    .add(new LatLng(latStartCalc, lngStartCalc), new LatLng(latEndCalc, lngEndCalc))
                    .width(5)
                    .geodesic(true)
                    .color(Color.BLUE));*/

            Toast toast = Toast.makeText(this, "distancia " + Math.round(distanceInMeters) , Toast.LENGTH_LONG);
            toast.show();
        }

    }

    private boolean isLocationEnabled() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        assert locationManager != null;
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void agregarMarcador(double lat, double lgn) {
        LatLng coordenada = new LatLng(lat, lgn);

        if (marcador != null) {
            marcador.remove();
        }
        marcador = mMap.addMarker(new MarkerOptions()
                .position(coordenada)
                .title("Posicion actual")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher)));

    }

    private void agregarMarcadorFirts(double lat, double lgn) {
        LatLng coordenada = new LatLng(lat, lgn);

        CameraUpdate miUbicacion = CameraUpdateFactory.newLatLngZoom(coordenada, 16);

        if (marcador != null) {
            marcador.remove();
        }
        marcador = mMap.addMarker(new MarkerOptions()
                .position(coordenada)
                .title("Posicion actual")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher)));

        mMap.animateCamera(miUbicacion);
    }

    private void actualizarUbicacion(Location location) {
        if (location != null) {

            if (trigger == 0){
                trigger++;
                lat2 = location.getLatitude();
                lng2 = location.getLongitude();

            }else if(trigger == 1){
                Location loc1 = new Location("");

                loc1.setLatitude(lat2);
                loc1.setLongitude(lng2);

                Location loc2 = new Location("");
                loc2.setLatitude(location.getLatitude());
                loc2.setLongitude(location.getLongitude());

                float distanceInMeters = loc1.distanceTo(loc2);

                Toast toast = Toast.makeText(this, "distancia " + Math.round(distanceInMeters), Toast.LENGTH_LONG);
                toast.show();

                lat2 = location.getLatitude();
                lng2 = location.getLongitude();

                trigger = 1;


            }

            lat = location.getLatitude();
            lng = location.getLongitude();
            //agregarMarcador(lat, lng);
        }
    }

    LocationListener locListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {

            actualizarUbicacion(location);
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    private void checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        isLocationEnabled();
    }

    private void showAlert() {

        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                    }
                });
        dialog.show();
    }

    @Override
    public void onClick(View view) {

        if(view == buttonNewCalculate){

            latStartCalc = 0.0;
            lngStartCalc = 0.0;

            latEndCalc = 0.0;
            lngEndCalc = 0.0;

            ((TextView) findViewById(R.id.time)).setText("");
            ((TextView) findViewById(R.id.distance)).setText("");

            mMap.clear();
            miUbicacion();


        }

        if(view == buttonCalcDistance){

            String origin      = String.valueOf(latStartCalc + "," + lngStartCalc);
            String destination = String.valueOf(latEndCalc + "," + lngEndCalc);


            try {
                new DirectionFinder(this, origin, destination).execute();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            calculateDistanceStartEnd();
        }
    }

    @Override
    public void onDirectionFinderStart() {
        progressDialog = ProgressDialog.show(this, "Please wait.",
                "Finding direction..!", true);

        if (originMarkers != null) {
            for (Marker marker : originMarkers) {
                marker.remove();
            }
        }

        if (destinationMarkers != null) {
            for (Marker marker : destinationMarkers) {
                marker.remove();
            }
        }

        if (polylinePaths != null) {
            for (Polyline polyline : polylinePaths) {
                polyline.remove();
            }
        }
    }

    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {

        progressDialog.dismiss();
        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();
        mMap.clear();

        for (Route route : routes) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 16));

            ((TextView) findViewById(R.id.time)).setText(route.duration.text);
            ((TextView) findViewById(R.id.distance)).setText(route.distance.text);

            originMarkers.add(mMap.addMarker(new MarkerOptions()
                    .title(route.startAddress)
                    .position(route.startLocation)));

            destinationMarkers.add(mMap.addMarker(new MarkerOptions()
                    .title(route.endAddress)
                    .position(route.endLocation)));

            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(Color.BLUE).
                    width(10);

            for (int i = 0; i < route.points.size(); i++)
                polylineOptions.add(route.points.get(i));

            polylinePaths.add(mMap.addPolyline(polylineOptions));
        }
    }


    private void fullScreen() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            getWindow().setFlags(
                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN
            );

        }
    }
}
